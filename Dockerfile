#
# ASSP - Anti-Spam SMTP Proxy Server
#
FROM iqhive/ubuntu-patched:1604
MAINTAINER Relihan Myburgh <rmyburgh@iqhive.com>

RUN mkdir -p /opt/assp;apt-get  update

RUN apt-get -y install clamav build-essential automake \
 autoconf libc6-dev-i386 linux-headers-generic libalgorithm-c3-perl \
 libalgorithm-dependency-perl libalgorithm-diff-perl \
 libalgorithm-diff-xs-perl libalgorithm-merge-perl \
 liblingua-stem-perl liblingua-stem-snowball-perl \
 liblingua-identify-perl libcompress-raw-zlib-perl \
 libio-compress-perl libdigest-md5-file-perl \
 libdigest-hmac-perl libdigest-perl libdigest-sha-perl \
 libio-digest-perl libcrypt-cbc-perl libcrypt-openssl-rsa-perl \
 libcrypt-openssl-dsa-perl libcrypt-openssl-x509-perl \
 libcrypt-ssleay-perl libcrypt-openssl-random-perl libcrypt-rc4-perl \
 tesseract-ocr poppler-utils xpdf imagemagick rar clamav-daemon; \
 find /var/cache/apt -name '*deb' | xargs -r rm
 
#
# First, install as many componnets we can from distro sources...
#

#RUN cpan ASSP::WordStem ?
#RUN cpan Stem::Snowball
#RUN cpan Lingua::Stem::Snowball
#RUN cpan Lingua::Identify
#RUN cpan install Archive::Zip
#RUN cpan Compress::Zlib
#RUN cpan Digest::MD5
#RUN cpan Digest::SHA1
#RUN cpan Crypt::CBC
#RUN cpan Crypt::OpenSSL::AES ?
#RUN cpan Crypt::RC4


# Required for Perl OCR and PDF modules
# 
# tesseract-ocr
# poppler-utils
# xpdf
# imagemagick


# RUN apt-get -y install libnet-cidr-perl libnet-cidr-lite-perl libnet-dns-perl libnet-subnet-perl libnet-ldap-perl libmail-spf-xs-perl libmail-spf-perl libnet-smtp-ssl-perl libnet-smtp-tls-perl libnet-smtps-perl libsys-syslog-perl liblogger-syslog-perl libunix-syslog-perl libdbd-csv-perl libdbd-sqlite3-perl libdbd-mysql-perl libdbd-mock-perl libemail-mime-perl libemail-mime-encodings-perl libfile-mimeinfo-perl libmime-lite-perl libemail-send-perl libemail-sender-perl libemail-valid-perl libmail-dkim-perl libmail-rfc822-address-perl libmail-spf-perl libmail-spf-xs-perl libmail-srs-perl libfile-chmod-perl libfile-find-rule-perl libfile-find-rule-perl-perl libfile-readbackwards-perl libfile-slurp-perl libfile-slurp-unicode-perl libfile-which-perl libmime-types-perl spf-tools-perl
RUN apt-get -y install libnet-cidr-perl libnet-cidr-lite-perl \
 libnet-subnet-perl libnet-ldap-perl \
 libmail-spf-xs-perl libmail-spf-perl libnet-smtp-ssl-perl \
 libnet-smtp-tls-perl libnet-smtps-perl libsys-syslog-perl \
 liblogger-syslog-perl libunix-syslog-perl libdbd-csv-perl \
 libdbd-sqlite3-perl libdbd-mysql-perl libdbd-mock-perl \
 libemail-mime-perl libemail-mime-encodings-perl libfile-mimeinfo-perl \
 libmime-lite-perl libemail-sender-perl \
 libemail-sender-transport-smtp-tls-perl \
 libemail-sender-transport-smtps-perl libemail-valid-perl \
 libmail-rfc822-address-perl libmail-spf-perl \
 libmail-spf-xs-perl libmail-srs-perl libfile-chmod-perl \
 libfile-find-rule-perl libfile-find-rule-perl-perl \
 libfile-readbackwards-perl libfile-slurp-perl \
 libfile-slurp-unicode-perl libfile-which-perl libmime-types-perl \
 spf-tools-perl; \
 find /var/cache/apt -name '*deb' | xargs -r rm

#RUN cpan Net::CIDR::Lite
#RUN cpan Net::IP::Match::Regexp
#RUN cpan Net::LDAP
#RUN cpan Net::SMTP ?
#RUN cpan Net::Syslog ?
#RUN cpan DBD::CSV
#RUN cpan DBD::File ?
#RUN cpan DBD::Log ?
#RUN cpan DBD::Mock
#RUN cpan DBD::Sprite ?
#RUN cpan DBD::Template ?
#RUN cpan Email::MIME::Modifier
#RUN cpan Email::Send
#RUN cpan Email::Valid
RUN cpan Mail::DKIM::Verifier
#RUN cpan Mail::SPF
#RUN cpan Mail::SRS
#RUN cpan File::chmod
#RUN cpan File::Find::Rule
#RUN cpan File::ReadBackwards
#RUN cpan File::Slurp
#RUN cpan File::Which
#RUN cpan MIME::Types
#RUN cpan -f -T Mail::SPF::Query ?


RUN apt-get -y install libclamav-client-perl libfile-scan-perl \
 libpdf-api2-perl libpdf-api2-simple-perl libcam-pdf-perl \
 libtext-pdf-perl libberkeleydb-perl libconvert-tnef-perl perlmagick \
 libgraphics-magick-perl libnumber-compare-perl libio-stringy-perl \
 libset-scalar-perl libscalar-defer-perl libschedule-cron-perl \
 libsmart-comments-perl libsys-cpu-perl libtext-glob-perl \
 libunicode-string-perl libio-socket-ssl-perl libemail-thread-perl \
 libmail-thread-perl libthread-pool-simple-perl \
 libthread-serialize-perl \
 liblinux-usermod-perl liblwp-mediatypes-perl \
 liblwp-protocol-https-perl libcrypt-ssleay-perl libio-all-lwp-perl \
 liblwp-online-perl liblwp-protocol-http-socketunix-perl \
 libtie-dbi-perl libyaml-perl libyaml-libyaml-perl libyaml-tiny-perl \
 libdbd-mysql libdbd-pgsql libdbd-ldap-perl \
 libdbd-mysql-perl libdbd-pg-perl \
 libregexp-optimizer-perl libclamav-dev; \
 find /var/cache/apt -name '*deb' | xargs -r rm
 
# libthread-queue-any-perl 
 
#RUN cpan -f -T File::Scan::ClamAV
#RUN cpan PDF::API2
#RUN cpan CAM::PDF
#RUN cpan Text::PDF
#RUN cpan BerkeleyDB
#RUN cpan Convert::TNEF
#RUN cpan Image::Magick
#RUN cpan Number::Compare
#RUN cpan PerlIO::scalar ?
#RUN cpan Schedule::Cron
#RUN cpan Smart::Comments
#RUN cpan Sys::CpuAffinity
#RUN cpan Text::Glob
#RUN cpan Unicode::GCString
#RUN cpan IO::Socket::SSL
#RUN cpan threads::shared
#RUN cpan Thread::State
#RUN cpan Linux::usermod
#RUN cpan LWP::Simple ?
#RUN cpan Tie::DBI
#RUN cpan YAML


# Part of base Perl install
#
# RUN cpan Time::HiRes

#
# Install remainaing components from CPAN
#

# Update CPAN first
RUN cpan install CPAN; \
 cpan install Sys::MemInfo; \
 cpan install LEOCHARRE::Dir; \ 
 cpan install LEOCHARRE::CLI; \
 cpan ROODE/Time-Format-1.12.tar.gz; \
 cpan install Net::SenderBase; \
 cpan install Tie::RDBM; \
 cpan Archive::Extract; \
 cpan install Net::DNS; \
 cpan install Net::SMTP

#RUN cpan File::PathInfo::Ext

#RUN cpan LEOCHARRE/File-PathInfo-1.27.tar.gz

#RUN cpan LEOCHARRE/PDF-Burst-1.19.tar.gz

RUN apt-get install wget; wget http://search.cpan.org/CPAN/authors/id/L/LE/LEOCHARRE/PDF-Burst-1.20.tar.gz;tar zxvf PDF-Burst-1.20.tar.gz;sed -i 's/defined %dat or warn/%dat or warn/g' PDF-Burst-1.20/lib/PDF/Burst.pm;cd PDF-Burst-1.20;perl Makefile.PL && make && make test && make install

RUN cpan PDF::GetImages; cpan -T PDF::OCR2; cpan Image::OCR::Tesseract

# RUN cpan LEOCHARRE/PDF-OCR2-1.20.tar.gz


#RUN apt-get install -y libdbd-mysql libdbd-pgsql libdbd-ldap-perl \
# libdbd-mysql-perl libdbd-pg-perl libarchive-extract-perl \
# libregexp-optimizer-perl libclamav-dev

RUN apt-get install -y pkg-config libssl-dev libarchive13

RUN cpan install -f Archive::Rar; \
 cpan install Archive::Zip; \
 cpan install Archive::Libarchive::XS; \
 cpan install HTML::Strip; \
 cpan install Crypt::SMIME

RUN cpan Convert::Scalar; \
 cpan Thread::State; \
 cpan -T File::Scan::ClamAV; \
 cpan Email::Send; \
 cpan Digest:SHA1; \
 cpan Sys::CpuAffinity; \
 cpan Text::Unidecode; \
 cpan -f -T Mail::SPF::Query; \
 cpan DBD::PgPP; \
 cpan DBIx::AutoReconnect;

RUN cpan install Thread::Queue; \
 cpan install threads; \
 cpan install Regexp::Optimizer

RUN cpan install Log::Log4perl

RUN apt-get -y install clamav-unofficial-sigs

RUN sed 's/min_sleep_time="60"/min_sleep_time="1"/g;s/honeynet.hdb//g;s/securiteinfo.hdb//g;s/securiteinfobat.hdb//g;s/securiteinfodos.hdb//g;s/securiteinfoelf.hdb//g;s/securiteinfohtml.hdb//g;s/securiteinfooffice.hdb//g;s/securiteinfopdf.hdb//g;s/securiteinfosh.hdb//g' -i /usr/share/clamav-unofficial-sigs/conf.d/00-clamav-unofficial-sigs.conf
RUN echo "#!/bin/bash\nRANDOM=1 /usr/sbin/clamav-unofficial-sigs\nexit 0\n" > /tmp/update-clamav-unofficial-sigs;chmod a+x /tmp/update-clamav-unofficial-sigs;/tmp/update-clamav-unofficial-sigs

RUN freshclam ; echo -n ;

COPY export/opt/assp/ /opt/assp/

COPY export/opt/startassp.sh /opt/startassp.sh

EXPOSE 25 55553 55555
ENTRYPOINT /opt/startassp.sh
