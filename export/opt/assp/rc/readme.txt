assp.small is written for RedHat 7.x , and many others I believe would work with
minor mods, if any.

assp - should work on any linux and is the best choice - do not forget to change the folder values in the script
assp.default - for debian and ununtu should be copied to /etc/default to define defaults

assp.bsd is for all BSD versions

The script 'assp' should be copied into /etc/init.d/   and then
linked to the appropriate run-level directories for starting up
and shutting down..   For example, I run at init 3 at startup, so
my script is linked to /etc/rc3.d/S79assp  (just before
S80sendmail)  and also to /etc/rc0.d/K31assp  (just after sendmail
shutdown).

Don't forget to set the permissions of assp and assp.pl 
(or best: all scripts) to 755

Folder and executable names and pathes has to be changed in all scripts!

Thomas