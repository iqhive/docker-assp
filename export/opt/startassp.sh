#!/bin/bash
cd /opt/assp

MASTER=NO
if [[ $ISMASTER"x" == "yx" ]]; then
MASTER=YES
fi
if [[ $ISMASTER"x" == "Yx" ]]; then
MASTER=YES
fi

if [[ $MASTER == "YES" ]]; then
sed -i "s/Q_MASTER_MODE/1/g" assp.cfg
sed -i "s/Q_SLAVE_MODE/0/g" assp.cfg
else
sed -i "s/Q_MASTER_MODE/0/g" assp.cfg
sed -i "s/Q_SLAVE_MODE/1/g" assp.cfg
fi

sed -i "s/THIS_HOSTNAME/$HOSTNAME/g" assp.cfg

exec /usr/bin/perl assp.pl . --nointchk:=1
